package com.example.mvvm_app2.model

data class RepositoriesList(val items: List<RepositoryData>)
