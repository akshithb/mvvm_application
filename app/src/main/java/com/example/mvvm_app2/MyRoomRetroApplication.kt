package com.example.mvvm_app2

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyRoomRetroApplication : Application() {
}